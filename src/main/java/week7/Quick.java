package week7;

public class Quick {

    public static void sort(int[] arr) {
        if (arr == null || arr.length == 0)
            return;

        int low = 0;
        int high = 0;
        if (low >= high)
            return;

        int middle = low + (high - low) / 2;
        int pivot = arr[middle];
        int i = low, j = high;
        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }

            while (arr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            sort(arr);

        if (high > i)
            sort(arr);
    }
}
