package RK2;

public class List {
    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[150];
    }

    public void add(Student student) {
        studentList[size++] = student;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }

    }


    //type: name_a name_l age gps
    //order: asc desc
    public void sort(String type, String order) {
        if(type.equals("name_a") && order.equals("asc")) {
            sort_name_a_asc();
        } else if(type.equals("name_a") && order.equals("desc")) {
            sort_name_a_desc();
        } else if(type.equals("name_l") && order.equals("asc")) {
            sort_name_l_asc();
        } else if(type.equals("name_l") && order.equals("desc")) {
            sort_name_l_desc();
        } else if(type.equals("age") && order.equals("asc")) {
            sort_age_asc();
        } else if(type.equals("age") && order.equals("desc")) {
            sort_age_desc();
        } else if(type.equals("gpa") && order.equals("asc")) {
            sort_gpa_asc();
        } else if(type.equals("gpa") && order.equals("desc")) {
            sort_gpa_desc();
        }
    }

    public void sort_name_a_asc() {
        System.out.println("name_a_asc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getName().codePointAt(0) < studentList[index].getName().codePointAt(0)){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }
    }

    public void sort_name_a_desc() {
        System.out.println("name_a_desc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getName().codePointAt(0) > studentList[index].getName().codePointAt(0)){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }

    }

    public void sort_name_l_asc() {
        System.out.println("name_l_asc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getName().length() < studentList[index].getName().length()){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }

    }

    public void sort_name_l_desc() {
        System.out.println("name_l_desc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getName().length() > studentList[index].getName().length()){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }
    }

    public void sort_age_asc() {
        System.out.println("age_asc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getAge() < studentList[index].getAge()){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }
    }

    public void sort_age_desc() {
        System.out.println("age_desc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getAge() > studentList[index].getAge()){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }
    }

    public void sort_gpa_asc() {
        System.out.println("gpa_asc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getAge() < studentList[index].getAge()){
                    index = j;
                }
            }

            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }
    }

    public void sort_gpa_desc() {
        System.out.println("gpa_desc: ");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getGpa() > studentList[index].getGpa()){
                    index = j;
                }
            }
            Student smallerNumber = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =smallerNumber;
        }

    }
}
