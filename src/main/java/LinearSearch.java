public class LinearSearch {
    static int search(int arr[], int n, int index)
    {
        for (int i = 0; i < n; i++) {
            if (arr[i] == index)
                return i;
        }

        return -1;
    }

    public static void main(String[] args)
    {
        int[] arr = { 1, 2, 3, 4, 5 };
        int n = arr.length;

        int find = 4;

        int index = search(arr, n, find);
        if (index == -1)
            System.out.println("Element is not present in the array");
        else
            System.out.println("Element found at position " + index);
    }
}
