package kz.aitu.week1;

import java.util.Scanner;

public class Practice8 {

    public static int recursion(int n, int b, int a) {
        if(b == 0) return a;
        a=a*n;
        return recursion(n, b-1,a);
    }
    public static void main(String[] args)  {
        int n;
        int b;
        int a=1;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        b = scanner.nextInt();
            System.out.println(recursion(n, b,a));
    }
}