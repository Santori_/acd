package kz.aitu.week1.quiz3;

public class LinkedListTest {


    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        //finding middle element of LinkedList in single pass
        Node middle ; //write your code here
        int length = 0;
        while( head != null){
            head = head.next;
            length++;
        }
        middle = linkedList.head;
        for(int i = 0; i < (length-1)/2 ; i++) {
            middle = middle.next;
        }
        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : "                                  + middle);

    }

}