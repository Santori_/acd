package kz.aitu.week1.quiz3;

public class LinkedList{ //nothing to change
    public Node head;
    public Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }
}