package kz.aitu.week1.week6.Tree;

public class Tree {
    private Node root;

    public Tree(){
        root = null;
    }

    public void insert(int data){
        root = insert(root,data);
    }

    private Node insert(Node root, int data){
        if (root==null){
            root = new Node(data);
        } else {
            if (data <= root.getData()){
                root.setLeft(insert(root.getLeft(), data));
            } else {
                root.setRight(insert(root.getRight(), data));
            }
        }
        return root;
    }

}


