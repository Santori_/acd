package kz.aitu.week1.week6.Hash;

public class Hash {
    private Node[] table;
    private int size;

    public void removeAtFirst(int key){
        Node Cur = null;
        Node tb = table[key];
        if (tb!=null){
            Cur = tb;
            table[key]=tb.getNext();
        } else {
            System.out.println("No value "+key);
        }
    }

    public void hash(int line){
        table = new Node[line];
        size = line;
    }

    public void insert(int key,int value){
        Node newCur = new Node(key, value);
        if(size>key){
            if(table[key]==null){
                table[key]=newCur;
            } else {
                table[key].setNext(newCur);
            }
        } else {
            System.out.println("Size: "+size);
        }
    }

    public void print(){
        for (int i =0;i<size;i++){
            Node Cur = table[i];
            while (Cur!=null){
                System.out.print(Cur.getValue()+" ");
                Cur = Cur.getNext();
            }
            System.out.println(" ");
        }
    }



}
