package kz.aitu.week1.week6.Hash;

public class Node {
    private int key;
    private int value;
    private Node next;

    public Node(int key, int value) {
        this.value = value;
        this.key = key;
        next = null;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}
