package kz.aitu.week1;

import java.util.Scanner;

public class Practice10 {

    public static int recursion(int d, int b, int a) {
        if(d==1) return d;
        if(a%d==0 && b%d == 0) return d;
        return recursion(d-1,b,a);


    }
    public static void main(String[] args)  {
        int a , b, c, d;
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();
        b = scanner.nextInt();
        if(b>a){
            c=a;
            a=b;
            b=c;
        }
        d=b;
        System.out.println(recursion(d, b,a));
    }
}