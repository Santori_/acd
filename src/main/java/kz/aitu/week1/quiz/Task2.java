package kz.aitu.week1.quiz;

import java.util.Scanner;

public class Task2 {

    public static int recursion(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter n please: ");
        int n = input.nextInt();
        if(n==0){
            return 0;
        }
        else{
            return Math.max(n,recursion());
        }
    }
    public static void main(String[] args) {
        System.out.println("Max: " + recursion());

    }
}

