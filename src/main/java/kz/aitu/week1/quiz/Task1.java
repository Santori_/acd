package kz.aitu.week1.quiz;

import java.util.Scanner;

public class Task1 {

    public static String recursion(int n) {
        if (n == 1) {
            return "1";
        } else {
            return recursion(n - 1) + " " + n;
        }
    }
    public static void main (String[]args){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter n please:");
            int n = scanner.nextInt();
            System.out.println(recursion(n));
    }
}