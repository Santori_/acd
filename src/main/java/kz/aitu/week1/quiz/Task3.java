package kz.aitu.week1.quiz;

import java.util.Scanner;

public class Task3 {

    public static void SecondMaximum(int FMax, int SMax) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n > 0) {
            if (FMax < n) {
                SecondMaximum(n, FMax);
            }
            if (SMax < n) {
                SecondMaximum(FMax, n);
            }
            else {
                SecondMaximum(FMax, SMax);
            }
        } else {
            System.out.println(SMax);
        }
    }
    public static void main(String[] args) {
        SecondMaximum(0, 0);
    }
}
