package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {

    public static int recursion(int n, int num1, int num2) {
        if(n==1) return 1;
        if(n==2) return num2;
        int t = num2;
        num2 = num1 + num2;
        num1= t;
        return recursion(n-1, num1, num2);
    }
    public static void main(String[] args)  {
        int n;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int num1=1;
        int num2=1;
        System.out.println("num : " + recursion(n, num1, num2));
    }
}