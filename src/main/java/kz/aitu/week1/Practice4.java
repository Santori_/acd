package kz.aitu.week1;

import java.util.Scanner;

public class Practice4 {

    public static int recursion(int n, int num1, int num2) {
        if(num1 == n+1) return num2;
        num2=num2*num1;
        return recursion(n, num1+1, num2);
    }

    public static void main(String[] args)  {
        int n;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int num1=1;
        int num2=1;

        System.out.println("n!= : " + recursion(n, num1, num2));
    }
}