package kz.aitu.week1.mid.Task3;

public class Stack {
    public static class stack { 
        int index = -1; 
        char items[] = new char[100];  
        public void push(char x) { 
            if (index == 99) { 
                System.out.println("Stack is full"); 
            } else { 
                items[index++] = x; 
            }
             
        }  
        char pop() { 
            if (index == -1) { 
                System.out.println("Error"); 
                return '\0'; 
            } else { 
                char element = items[index]; 
                index--; 
                return element; 
            } 
        }  
        boolean isEmpty() { 
            return (index == -1) ? true : false; 
        } 
    }   
    static boolean isMatched(char character1, char character2) { 
        if (character1 == '(' && character2 == ')') 
        return true; 
        else if (character1 == '{' && character2 == '}') 
        return true; 
        else if (character1 == '[' && character2 == ']') 
        return true; 
        else {
            return false; 
        }
    }  
    public static boolean isBalanced(char exp[]) {  
        stack stack = new stack();   
        for (int i = 0; i < exp.length; i++) { 
            if (exp[i] == '{' || exp[i] == '(' || exp[i] == '[') 
            stack.push(exp[i]); 
            if (exp[i] == '}' || exp[i] == ')' || exp[i] == ']') {   
                if (stack.isEmpty()) { 
                    return false; 
                } else if (!isMatched(stack.pop(), exp[i])) { 
                    return false; 
                } 
            }  
        }  
        if (stack.isEmpty()) 
        return true; 
        else { 
            return false; 
        } 

    } 
}
