package kz.aitu.week1.mid.LinkedLIst;

public class LinkedList {
    Node head; 
    public class Node { 
        int tail; 
        Node next; 
        Node(int i) { 
            tail = i; 
            next = null; 
        } 
    } 
    public void add(int data) { 
        Node cur = new Node(data); 
        cur.next = head; 
        head = cur; 
    }   
    public void removeAt(int position) { 
        if (head == null) 
        return; 
        Node temp = head; 
        if (position == 0) { 
            head = temp.next; 
            return; 
        } 
        for (int i=0; temp!=null && i<position-1; i++) 
        temp = temp.next;  
        if (temp == null || temp.next == null) 
        return; 
        Node next = temp.next.next; 
        temp.next = next; 
    }   
    public void List(){ 
        Node cur = head; 
        while (cur.next != null) { 
            System.out.println(cur.tail); 
            cur= cur.next; 
        } 
        System.out.println(cur.tail); 
    }
}
