package kz.aitu.week1.week2;

import java.util.Scanner;

public class Practice702 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (1<=n && n<=100) reverse(n);
    }

    public static void reverse(int n){
        Scanner scanner1 = new Scanner(System.in);
        if (n!=0) {
            int e = scanner1.nextInt();
            reverse(n-1);
            System.out.print(e + " ");
        }
        return;
    }
}
