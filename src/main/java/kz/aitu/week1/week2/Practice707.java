package kz.aitu.week1.week2;

import java.util.Scanner;

public class Practice707 {
    public static void combinations(String a, String b){
        int n;
        n = b.length();
        if (n==0) System.out.println(a);
        else {
            for (int i=0; i<n; i++){
                combinations(a+b.charAt(i), b.substring(0,i)+b.substring(i+1,n));
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a;
        a = scanner.nextLine();
        combinations("", a);
    }
}

