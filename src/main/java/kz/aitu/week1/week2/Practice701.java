package kz.aitu.week1.week2;

import java.util.Scanner;

public class Practice701 {
    public static int recursion(int n) {
        if (n==1 || n==0) return n;
        return recursion(n-1)+recursion(n-2);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();

        System.out.println(recursion(n));
    }
}
