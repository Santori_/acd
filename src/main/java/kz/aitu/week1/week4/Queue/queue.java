package kz.aitu.week1.week4.Queue;

public class Queue {
    Node head;
    Node tail;

    public void add(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
        } else {
            Node temp=head;
            head=newNode;
            newNode.next = temp;
        }
    }

    public Node poll() {
        Node beforeHead = head;
        if (head == null) {
            return null;
        } else {
            head=head.next;
            return beforeHead;
        }


    }

    public int size() {
        Node current = head;
        int sum=1;
        while (current != null) {
            current = current.next;
            sum++;

        }
        return sum;
    }

    public int peek() {
        if (head == null) {
            return -1;
        } else{
            return head.data;
        }
    }

    public boolean empty() {
        if (head == null) {
            return true;
        } else {
            return false;
        }
    }
}
