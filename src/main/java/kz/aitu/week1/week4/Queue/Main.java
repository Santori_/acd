package kz.aitu.week1.week4.Queue;

public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue();

        queue.add(1);
        queue.add(2);
        queue.add(3);

        System.out.println("peek: " + queue.peek());

        queue.poll();

        System.out.println("size: " + queue.size());

        System.out.println("is stack empty? " + queue.empty());


    }
}

