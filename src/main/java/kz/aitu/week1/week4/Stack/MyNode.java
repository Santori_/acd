package kz.aitu.week1.week4.Stack;

public class MyNode {
    int data;
    MyNode next;

    public MyNode(int data) {
        this.data = data;
        next = null;
    }
}

