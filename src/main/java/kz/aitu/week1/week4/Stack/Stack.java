package kz.aitu.week1.week4.Stack;
import kz.aitu.week1.week4.Stack.MyNode;

public class Stack {
    private MyNode tail;
    private MyNode top;
    private int size;

    public Stack() {
        tail = top = null;
        size = 0;
    }

    public int pop(){
        MyNode popped = tail;
        for (int i = 0; i < size-1; i++) {
            popped = popped.next;
        }
        int c = popped.next.data;
        popped.next = null;
        top = popped;
        size--;
        return c;
    }

    public void push(int data) {
        size++;
        MyNode newNode = new MyNode(data);
        if (top == null) {
            top = newNode;
        } else {
            top.next = newNode;
            top = top.next;
        }
    }

    public boolean empty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return size;
    }

    public int top() {
        if (top == null) {
            return 0;
        } else {
            return top.data;
        }
    }
    public int get(int index) {
        MyNode node = tail;
        for (int i = 0; i < size; i++) {
            node = node.next;
        }
        return node.data;
    }
}


